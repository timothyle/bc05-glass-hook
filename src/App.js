import { useState } from "react";
import "./App.css";
import RenderGlass from "./Glass/RenderGlass";
import GlassModel from "./Glass/Model";
import {dataGlasses} from "./Glass/dataGlass";

const data = {
  id: "",
  price: "",
  name: "",
  url: "",
  desc:"",
};

function App() {
  const [glassItem, setGlass] = useState(data);
  return (
     <div className="py-5 mx-auto bg-info text-white display-4 justify-content-center align-items-center">
            Parker & Co Eyewear
        <div className="row justify-content-center">
          <img
            className="col-5 mx-auto"
            style={{ width: "200px", height: "500px" }}
            src="./glassesImage/model.jpg"
            alt=""
          /></div>
        <div className="row align-items-center">
          <div className="col-6">
            <GlassModel glassItem={glassItem} />
          </div>
          <div className="col-6">
            <RenderGlass dataGlasses={dataGlasses} setGlass={setGlass} />
          </div>
        </div>
    </div>
  );
}

export default App;

