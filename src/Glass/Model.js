import React from "react";

export default function GlassModel ({ glassItem }) {
    let {url,name,price, desc} = glassItem;
    return (
      <div>
        <div className="py-5 mx-auto bg-info text-white display-4">
            Parker & Co Eyewear
        </div>
        <div className="row justify-content-center align-items-center" style={{ width: "1000px", height: "500px"}} >
          <img
            className="col-5"
            style={{ width: "200px", height: "500px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
          <div className="detail">
                <img src={url} alt="kinh" className="glass-img" />
            <div className="detail__content bg-dark">
                <p className="detail__name text-danger">{name}</p>
                <p className="text-warning">${price}</p>
                <p className="text-white">{desc}</p>
            </div>
           </div>
        </div>
      </div>
    );
  }
